﻿using System;
using System.Collections;

namespace Kabak
{
    public class StudentList : IEnumerable
    {
        public int Size { get; set; }
        public Student[] ListOfStudent { get; set; }

        public StudentList()
        {
            Size = 0;
            ListOfStudent = new Student[0];
        }

        public void AddStudent(Student newStudent)
        {
            var tmpStudentArray = new Student[Size + 1];

            for (int i = 0; i < Size; i++)            
                tmpStudentArray[i] = ListOfStudent[i];
            tmpStudentArray[Size] = newStudent;
            ListOfStudent = tmpStudentArray;
            Size++;
            tmpStudentArray = null;
        }
        public void RemoveStudent(int index)
        {
            if (!(index >= 0 && index <= ListOfStudent.Length - 1))            
                return;            

            var tmpStudentArray = new Student[Size - 1];
            for (int i = 0; i < index; i++)            
                tmpStudentArray[i] = ListOfStudent[i];
            

            for (int i = index + 1; i < Size; i++)          
                tmpStudentArray[i - 1] = ListOfStudent[i];

            ListOfStudent = tmpStudentArray;
            Size--;
            tmpStudentArray = null;
        }
        public void GroupDelete(int number, String str)
        {
            switch (number)
            {
                case 1:

                    for (int i = 0; i < Size; i++)
                    {
                        if (ListOfStudent[i].GroupIndex.Equals(str))
                        {
                            RemoveStudent(i);
                        }
                    }
                    break;

                case 2:

                    for (int i = 0; i < Size; i++)
                    {
                        if (ListOfStudent[i].Specialization.Equals(str))
                        {
                            RemoveStudent(i);
                        }
                    }

                    break;
                case 3:

                    for (int i = 0; i < Size; i++)
                    {
                        if (ListOfStudent[i].Faculty.Equals(str))
                        {
                            RemoveStudent(i);
                        }
                    }

                    break;
            }
        }
        public void ShowByIndex(int index)
        {
            if (index >= Size || index < 0)
            {
                return;
            }

            Io.OutputStudent(ListOfStudent[index]);
        }
        public void Sort()
        {
            Student temp;

            for (int write = 0; write < ListOfStudent.Length; write++)
            {
                for (int sort = 0; sort < ListOfStudent.Length - 1; sort++)
                {
                    if (String.Compare(ListOfStudent[sort].LastName, ListOfStudent[sort + 1].LastName) > 0)
                    {
                        temp = ListOfStudent[sort + 1];
                        ListOfStudent[sort + 1] = ListOfStudent[sort];
                        ListOfStudent[sort] = temp;
                    }
                }
            }

        }
        public void ShowAll()
        {
            Console.WriteLine("-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
            Console.WriteLine("|  №  |     Surname     |      Name       |    Patronymic    |  Date of birth  |  Enter date  |  Group index  |            Faculty            |          Specialty        |    Academic performance   |");
            Console.WriteLine("-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
            int i = 0;

            foreach (Student student in ListOfStudent)
            {
                String output = String.Format("| {0,-3} | {1,-15} | {2,-15} | {3,-16} | {4,-15} | {5,-12} | {6,-13} | {7,-29} | {8,-25} | {9,-25} |",
                    (++i), student.LastName, student.FirstName, student.Patronymic, student.BirthDate.ToString("dd.MM.yyyy"), student.EnterDate.ToString("dd.MM.yyyy"), student.GroupIndex, student.Faculty, student.Specialization, student.Performance);
                Console.WriteLine(output);
            }
            Console.WriteLine("-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");

        }
        public void EditByIndex(int index)
        {

            int day;
            int month;
            int year;

            int choice = 1;
            Console.WriteLine("Choose what you want to do:\n");
            Console.WriteLine("1) Change last name");
            Console.WriteLine("2) Change first name");
            Console.WriteLine("3) Change Patronymic");
            Console.WriteLine("4) Change birth date");
            Console.WriteLine("5) Change enter date");
            Console.WriteLine("6) Change group index");
            Console.WriteLine("7) Change faculty");
            Console.WriteLine("8) Change specializstion");
            Console.WriteLine("9) Change perfomance");
            choice = Io.InputInt("choise");


            switch (choice)
            {
                case 1:
                    ListOfStudent[index].LastName = Io.InputName("\n\tLastName");
                    break;
                case 2:
                    ListOfStudent[index].FirstName = Io.InputName("\n\tFirstName");
                    break;
                case 3:
                    ListOfStudent[index].Patronymic = Io.InputName("\n\tPatronymic");
                    break;
                case 4:
                    day = Io.InputInt("day");
                    month = Io.InputInt("month");
                    year = Io.InputInt("year");
                    ListOfStudent[index].BirthDate = new DateTime(year, month, day);
                    break;
                case '5':
                    day = Io.InputInt("day");
                    month = Io.InputInt("month");
                    year = Io.InputInt("year");
                    ListOfStudent[index].EnterDate = new DateTime(year, month, day);
                    break;
                case '6':
                    ListOfStudent[index].GroupIndex = Io.InputString("\n\tGroupIndex");
                    break;
                case '7':
                    ListOfStudent[index].Faculty = Io.InputString("\n\tFaculty");
                    break;
                case '8':
                    ListOfStudent[index].Specialization = Io.InputString("\n\tSpecialization");
                    break;
                case '9':
                    ListOfStudent[index].Performance = Io.InputInt("\n\tPerformance");
                    break;
            }
        }
        public IEnumerator GetEnumerator()
        {
            if (ListOfStudent != null)
            {
                for (int index = 0; index < ListOfStudent.Length; index++)
                {
                    yield return ListOfStudent[index];
                }
            }

        }
    }
}
