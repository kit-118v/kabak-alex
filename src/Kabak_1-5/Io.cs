﻿using System;
using System.IO;
using System.Xml.Serialization;

namespace Kabak
{
    public static class Io
    {
        private static string _errorMessage = " You entered the wrong date. Please check and try again";

        public static string InputName(string fieldName)
        {
            Console.WriteLine(fieldName + ":");
            string name = Console.ReadLine();

            while (!RegexChecker.ValidateName(name))
            {
                Console.WriteLine(_errorMessage);
                name = Console.ReadLine();
            }

            return name;
        }

        public static DateTime InputDate(String fieldName)
        {
            Console.WriteLine( fieldName + ":");

            while (true)
            {
                try
                {
                    Console.WriteLine("Enter year:");
                    int year = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine("Enter month:");
                    int month = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine("Enter day:");
                    int day = Convert.ToInt32(Console.ReadLine());
                    return new DateTime(year, month, day);
                }
                catch (Exception e)
                {
                    Console.WriteLine("An exception occured while date input:" + e.Message);
                }
            }

        }

        public static string InputString(string fieldName)
        {
            Console.WriteLine( fieldName + ":");
            string sentence = Console.ReadLine();

            while (!RegexChecker.ValidateString(sentence))
            {
                Console.WriteLine(_errorMessage);
                sentence = Console.ReadLine();
            }

            return sentence;
        }

        public static int InputInt(string fieldName)
        {
            Console.WriteLine( fieldName + ":");
            while (true)
            {
                if (int.TryParse(Console.ReadLine(), out int value))
                {
                    return value;
                }

                Console.WriteLine("Error: wrong type");
            }
        }
        public static Student Insert()
        {
            var newStudent = new Student();

            newStudent.FirstName = InputName("\n\tFirstName");
            newStudent.LastName = InputName("\n\tLastName");
            newStudent.Patronymic = InputName("\n\tPatronymic");
            newStudent.BirthDate = InputDate("\n\tBirth date");
            newStudent.EnterDate = InputDate("\n\tEnrollment date");
            newStudent.GroupIndex = InputString("\n\tGroup index");
            newStudent.Faculty = InputString("\n\tFaculty name");
            newStudent.Specialization = InputString("\n\tSpecailization");
            newStudent.Performance = InputInt("\n\tPerfomance");

            return newStudent;
        }

        public static void WriteStContainerToFile(string path, StudentList writtenContainer)
        {

            try
            {
                using (StreamWriter sw = new StreamWriter(path, false, System.Text.Encoding.Default))
                {

                    foreach (var i in writtenContainer)
                    {
                        sw.WriteLine(i.ToString());
                    }

                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

        }

        public static void ReadStContainerFromFile(string path, StudentList readContainer)
        {
            try
            {
                using (StreamReader sr = new StreamReader(path, System.Text.Encoding.Default))
                {
                    string line;
                    while ((line = sr.ReadLine()) != null)
                    {
                        var infoStudent = line.Split(new char[] { '|' });

                        readContainer.AddStudent(new Student(infoStudent[0],
                            infoStudent[1],
                            infoStudent[2],
                            DateTime.Parse(infoStudent[3]),
                            DateTime.Parse(infoStudent[4]),
                            infoStudent[5],
                            infoStudent[6],
                            infoStudent[7],
                            int.Parse(infoStudent[8])));
                    }
                }
            }
            catch (Exception e) { }
        }

        public static void Save(StudentList data)
        {
            XmlSerializer formatter = new XmlSerializer(typeof(Student[]));

            using (FileStream fs = new FileStream("slaves.xml", FileMode.OpenOrCreate))
            {
                formatter.Serialize(fs, data.ListOfStudent);
            }

        }

        public static void Download(StudentList data)
        {
            XmlSerializer formatter = new XmlSerializer(typeof(Student[]));
            using (FileStream filestream = new FileStream("slaves.xml", FileMode.OpenOrCreate))
            {
                data.ListOfStudent = (Student[])formatter.Deserialize(filestream);
            }

        }

        public static void OutputStudent(Student printedStudent)
        {
            Console.WriteLine(printedStudent);
        }

    }
}
