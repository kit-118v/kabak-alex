﻿using System;

namespace Kabak
{
    public static class Helper
    {
        public static String Age(Student data)
        {
            DateTime today = DateTime.Today;
            DateTime b = data.BirthDate;

            return $"Age in days: {(DateTime.Today - b).Days}";
        }
        public static String GetCourse(Student data)
        {
            int course, semester;
            course = DateTime.Today.Year - data.EnterDate.Year + 1;
            if (DateTime.Today.Month >= 7 && DateTime.Today.Month <= 12)
            {
                semester = course * 2 - 1;
            }
            else
            {
                semester = course * 2;
            }

            return $"Сourse: {course}, semester : {semester}";
        }
        public static int GetAge(DateTime date)
        {
            DateTime today = DateTime.Today;
            TimeSpan old = today.Subtract(date);
            var d = new DateTime(old.Ticks);

            return d.Year - 1;
        }

        public static int AveragePerfomance(int number, String str, Student[] students)
        {
            int count = 0, progress = 0, av;

            switch (number)
            {
                case 1:

                    for (int i = 0; i < students.Length; i++)
                    {
                        if (students[i].GroupIndex.Equals(str))
                        {
                            count++;
                            progress += students[i].Performance;
                        }
                    }
                    break;

                case 2:

                    for (int i = 0; i < students.Length; i++)
                    {
                        if (students[i].Specialization.Equals(str))
                        {
                            count++;
                            progress += students[i].Performance;
                        }
                    }
                    break;

                case 3:

                    for (int i = 0; i < students.Length; i++)
                    {
                        if (students[i].Faculty.Equals(str))
                        {
                            count++;
                            progress += students[i].Performance;
                        }
                    }
                    break;

            }
            av = progress / count;
            return av;
        }

        public static int AverageAge(int number, String str, Student[] students)
        {
            int count = 0, age = 0, av;

            switch (number)
            {
                case 1:

                    for (int i = 0; i < students.Length; i++)
                    {
                        if (students[i].GroupIndex.Equals(str))
                        {
                            count++;
                            age += Helper.GetAge(students[i].BirthDate);
                        }
                    }

                    break;
                case 2:

                    for (int i = 0; i < students.Length; i++)
                    {
                        if (students[i].Specialization.Equals(str))
                        {
                            count++;
                            age += Helper.GetAge(students[i].BirthDate);
                        }
                    }

                    break;
                case 3:

                    for (int i = 0; i < students.Length; i++)
                    {
                        if (students[i].Faculty.Equals(str))
                        {
                            count++;
                            age += Helper.GetAge(students[i].BirthDate);
                        }
                    }

                    break;
            }

            av = age / count;
            return av;
        }

    }
}
