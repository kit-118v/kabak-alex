﻿using System;
using System.Text;

namespace Kabak
{
    public static class Menu
    {
        delegate void Message();
        public static void Start()
        {
            Message mes = PrintMenu;
            int tmpIndex;
            int choice = 1;
            var students = new StudentList();
            Student tmpStudent;
            const string path = @"D:\Slaves.txt";

            Console.WriteLine("Students list");

            while (choice != 0)
            {
                mes();
                Console.Write("Make a choice: ");
                choice = Convert.ToInt32(Console.ReadLine());
                switch (choice)
                {
                    case 1:
                        students.ShowAll();
                        break;
                    case 2:
                        tmpStudent = Io.Insert();
                        students.AddStudent(tmpStudent);
                        break;
                    case 3:
                        tmpIndex = Io.InputInt("index to delete element");
                        students.RemoveStudent(tmpIndex);
                        break;
                    case 4:
                        tmpIndex = Io.InputInt("index to show element");
                        students.ShowByIndex(tmpIndex);
                        break;
                    case 5:
                        students.Sort();
                        break;
                    case 6:
                        Io.WriteStContainerToFile(path, students);
                        break;
                    case 7:
                        Io.ReadStContainerFromFile(path, students);
                        break;
                    case 8:
                        tmpIndex = Io.InputInt("index to edit element");
                        students.EditByIndex(tmpIndex);
                        break;
                    case 9:
                        SpecialOut(students);
                        break;
                    case 10:
                        Io.Save(students);
                        break;
                    case 11:
                        Io.Download(students);
                        break;
                }
            }

        }

        public static void SpecialOut(StudentList data)
        {
            int choice = 1, choice2 = 1;
            int tmpIndex;
            StringBuilder sb = new StringBuilder();


            while (choice != 0)
            {
                Menu.PrintSpecialMenu();
                Console.Write("Make a choice: ");
                choice = Convert.ToInt32(Console.ReadLine());

                switch (choice)
                {
                    case 1:
                        tmpIndex = Io.InputInt("index of student");
                        sb.Append("\n\nFaculty:" + data.ListOfStudent[tmpIndex].Faculty);
                        sb.Append("\n" + data.ListOfStudent[tmpIndex].Specialization);
                        sb.Append("-");
                        sb.Append(data.ListOfStudent[tmpIndex].EnterDate.Year);
                        sb.Append(data.ListOfStudent[tmpIndex].GroupIndex);
                        sb.AppendLine();
                        Console.WriteLine(sb.ToString());

                        break;
                    case 2:
                        tmpIndex = Io.InputInt("index of student");
                        Console.WriteLine(Helper.GetCourse(data.ListOfStudent[tmpIndex]));
                        break;
                    case 3:
                        tmpIndex = Io.InputInt("index of student");

                        Console.WriteLine(Helper.Age(data.ListOfStudent[tmpIndex]));
                        break;
                    case 4:
                        Console.WriteLine("Choose:\n");
                        Console.WriteLine("1 - By group");
                        Console.WriteLine("2 - By specialty");
                        Console.WriteLine("3 - By faculty");
                        choice2 = Io.InputInt("Choise");
                        Console.WriteLine("Average age: " + Helper.
                            AverageAge(choice2, Io.InputString("Group|Faculty|Specailty"), data.ListOfStudent));

                        break;
                    case 5:
                        Console.WriteLine("Choose:\n");
                        Console.WriteLine("1 - By group");
                        Console.WriteLine("2 - By specialty");
                        Console.WriteLine("3 - By faculty");
                        choice2 = Io.InputInt("Choise");
                        Console.WriteLine("Average perfomance: " + Helper.
                            AveragePerfomance(choice2, Io.InputString("Group|Faculty|Specailty"), data.ListOfStudent));

                        break;

                    case 6:
                        Console.WriteLine("Choose:\n");
                        Console.WriteLine("1 - By group");
                        Console.WriteLine("2 - By specialty");
                        Console.WriteLine("3 - By faculty");
                        choice2 = Io.InputInt("Choise");
                        data.GroupDelete(choice2, Io.InputString("Group|Faculty|Specailty"));

                        break;
                }
            }
        }

        public static void PrintSpecialMenu()
        {
            Console.WriteLine("Choose what you want to do:\n");
            Console.WriteLine("1) Index of group, specialty, faculty");
            Console.WriteLine("2) Course and semestr till the moment");
            Console.WriteLine("3) Age of student");
            Console.WriteLine("4) Average age");
            Console.WriteLine("5) Average Performance");
            Console.WriteLine("6) Group delete");
            Console.WriteLine("\n0) Exit");
        }
        private static void PrintMenu()
        {
            Console.WriteLine("\n\nMenu for working with a group of students:\n");
            Console.WriteLine("1)  Show data for all students");
            Console.WriteLine("2)  Add new student");
            Console.WriteLine("3)  Delete student by index");
            Console.WriteLine("4)  Show by index");
            Console.WriteLine("5)  Sort");
            Console.WriteLine("6)  Write to file");
            Console.WriteLine("7)  Read from file");
            Console.WriteLine("8)  Edit student by index");
            Console.WriteLine("9)  Special output");
            Console.WriteLine("10) Save");
            Console.WriteLine("11) Download");
            Console.WriteLine("\n0) Exit");
        }
    }
}
